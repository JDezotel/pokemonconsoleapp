﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Newtonsoft.Json.Linq;


namespace Pokemon
{
    class Program
    {
        static void Main(string[] args)
        {
            using (WebClient wc = new WebClient())
            {

                Console.WriteLine("Welcome to Jeremy's Battle Pokedex!");
                Thread.Sleep(1000);
                Console.WriteLine();
                Console.WriteLine("Enter the name of the your Pokemon or one you're facing and I'll give you it's strengths and weaknesses! ");
                Thread.Sleep(1500);
                Console.WriteLine();
                Console.WriteLine();
                Console.Write("Pokemon Name: ");
                string PokemonName = Console.ReadLine().ToLower();
                Console.WriteLine();
                Console.WriteLine();
                while (PokemonName.ToUpper().Trim() != "EXIT")
                {
                    try
                    {
                        // Creates url to query, passing in the entered pokemon name and then making request
                        var json = wc.DownloadString("https://pokeapi.co/api/v2/pokemon/" + PokemonName);

                        // turns json into a .NET object to be manipulated
                        dynamic objA = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
                        // gets the pokemon's type
                        var type = objA["types"].ToString().Split(new string[] { "name" }, StringSplitOptions.None)[1].Split(',')[0].Replace(":", "").Replace(" ", "").Replace("\"", "");
                        //makes API request bsed on entered pokemon type, and then saves "damage_relations" object
                        json = wc.DownloadString("https://pokeapi.co/api/v2/type/" + type);
                        objA = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
                        var damage_relations = objA["damage_relations"];//.damage_relations[];
                        Console.WriteLine("Great Choice! Let's see.....");
                        Thread.Sleep(2500);
                        Console.WriteLine();
                        Console.WriteLine("Pokemon Name: " + PokemonName);
                        Console.Write("Pokemon type : " + type);
                        Thread.Sleep(1000);
                        Console.WriteLine();
                        Console.WriteLine();
                        Console.Write("Weak against:");
                        Thread.Sleep(750);
                        Console.WriteLine();

                        // loop through types that are strong against your searched pokemon and print to console
                        int i = 1;
                        foreach (var name in damage_relations["double_damage_from"])
                        {
                            Console.Write(i.ToString() + ". " + name["name"].ToString());
                            Console.WriteLine();
                            i++;
                        }
                        Console.WriteLine();
                        Thread.Sleep(1000);
                        Console.Write("Strong against:");
                        Thread.Sleep(750);
                        Console.WriteLine();

                        // loop through types that are weak against your searched pokemon and print to console
                        i = 1;
                        foreach (var name in damage_relations["double_damage_to"])
                        {
                            Console.Write(i.ToString() + ". " + name["name"].ToString());
                            Console.WriteLine();
                            i++;
                        }
                        Console.WriteLine();
                        Thread.Sleep(1000);
                        Console.Write("Type exit to quit or Enter another Pokemon name : ");
                        PokemonName = Console.ReadLine().ToLower();
                    }

                    // Exceptions- GOTTA CATCH EM ALL!!!!!
                    catch (Exception ex)
                    {
                        Console.WriteLine();
                        Console.Write(ex.Message);
                        Console.WriteLine();
                        Console.WriteLine();
                        Console.WriteLine("What kinda pokemon is that?!?!");
                        Console.WriteLine();
                        Console.WriteLine();
                        Console.Write("Type exit to quit or Enter another Pokemon name : ");
                        PokemonName = Console.ReadLine().ToLower();
                    }
                    
                }
                
            }
            
        }
    }
}
